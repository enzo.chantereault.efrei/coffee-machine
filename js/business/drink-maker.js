import Order from '../models/entities/order/order.entity.js';
import OrderType from '../models/enums/order-type.enum.js';
import Report from '../models/entities/report/report.entity.js';
import {
    addition,
    substraction,
    multiplication
} from '../services/utils.service.js';

export default class DrinkMaker {

    constructor(drinkMakerId) {
        this.drinkMakerId = drinkMakerId;
        this.reportId = `Drink maker report_${this.drinkMakerId}`;
    }

    makeOrder = (orderProtocol) => {
        const oderParts = orderProtocol.split(':');
        let orderEntity = new Order(oderParts[0]);
        const insertedEuros = Number(oderParts[2]);

        if (!this.isMoneyEnough(orderEntity.price, insertedEuros)) {
            return this.makeOrder(`M:missing ${substraction(orderEntity.price, insertedEuros)} euros to buy 1 ${orderEntity.orderTypeText}`);
        }

        if (orderEntity.orderTypeCode == OrderType.error) {
            orderEntity.message = orderEntity.orderTypeText;
        } else if (orderEntity.orderTypeCode == OrderType.message) {
            orderEntity.message = oderParts[1];
        } else {
            orderEntity.sugar = oderParts[1];
            orderEntity.message = this.writeSuccessMessage(orderEntity);
            this.addToReports(orderEntity);
        }

        return orderEntity.message;
    }

    writeSuccessMessage = (orderEntity) => {
        orderEntity.message = `makes 1 ${orderEntity.orderTypeText} `;
        if (orderEntity.orderTypeCode != OrderType.orange) {
            orderEntity.message += `with ${orderEntity.sugar ? orderEntity.sugar : 'no'} sugar`;
            if (this.isSugarPlural(orderEntity.sugar)) {
                orderEntity.message += 's';
            }
            orderEntity.message += ` ${orderEntity.hasStick ? 'and a stick' : '- and therefore no stick'}`;
        }
        return orderEntity.message;
    }

    isMoneyEnough = (price, money) => {
        if (price > money)
            return false;
        return true;
    }

    isSugarPlural = (numberOfSugar) => {
        if (numberOfSugar > 1)
            return true;
        return false;

    }

    //#region Report

    printReports = () => {
        console.log('Reports:', this.getReports());
    }

    addToReports = (orderEntity) => {
        let reports = this.getReports();

        if (!reports || reports?.length == 0) {
            reports = [{ total: 0 }];
        }

        const report = this.findReportByTypeCode(reports, orderEntity.orderTypeCode);
        if (!report) {
            reports.push(this.initReport(orderEntity));
        } else {
            report.quantity++;
            report.total = multiplication(report.quantity, report.unitPrice);
            report.updatedDate = new Date();
        }

        reports[0].total = this.calculateTotalSoldSoFar(reports);

        localStorage.setItem(this.reportId, JSON.stringify(reports));
    }

    getReports = () => {
        return JSON.parse(localStorage.getItem(this.reportId));
    }

    initReport(orderEntity) {
        return new Report(orderEntity);
    }

    findReportByTypeCode = (reports, orderTypeCode) => {
        for (var i = 0; i < reports.length; i++) {
            if (reports[i].typeCode == orderTypeCode) {
                return reports[i];
            }
        }
        return;
    }

    calculateTotalSoldSoFar = (reports) => {
        let reportsTotal_buff = 0 - reports[0].total;

        reports.forEach(report => {
            reportsTotal_buff = addition(reportsTotal_buff, report.total);
        });

        return reportsTotal_buff;
    }

    //#endregion

};