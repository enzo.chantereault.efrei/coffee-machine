const OrderType = Object.freeze(
    {
        tea: "T",
        coffee: "C",
        chocolate: "H",
        orange: "O",
        hotTea: "Th",
        hotCoffee: "Ch",
        hotChocolate: "Hh",
        message: "M",
        error: 500,
    }
);

export default OrderType;