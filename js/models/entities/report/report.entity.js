export default class Report {

    constructor(orderEntity) {
        this.typeCode = orderEntity.orderTypeCode;
        this.typeText = orderEntity.orderTypeText;
        this.unitPrice = orderEntity.price;
        this.quantity = 1;
        this.total = orderEntity.price;
        this.createdDate = new Date();
        this.updatedDate = null;
    }

};