import OrderType from '../../enums/order-type.enum.js';
import { capitalizeFirstLetter } from '../../../services/utils.service.js';

export default class Order {

    constructor(orderType) {
        this._orderTypeCode = capitalizeFirstLetter(orderType);
        this.orderType = this.orderTypeCode;
    }

    get orderTypeCode() {
        return this._orderTypeCode;
    }

    get orderTypeText() {
        return this._orderTypeText;
    }

    set orderType(orderTypeEnum) {
        if (Object.values(OrderType).includes(orderTypeEnum)) {
            switch (true) {
                case orderTypeEnum == OrderType.tea || orderTypeEnum == OrderType.hotTea:
                    this._price = 0.4;
                    if (orderTypeEnum == OrderType.tea) {
                        this._orderTypeText = "tea";
                    } else {
                        this._orderTypeText = "extra hot tea";
                    }
                    break;
                case orderTypeEnum == OrderType.coffee || orderTypeEnum == OrderType.hotCoffee || orderTypeEnum == OrderType.orange:
                    this._price = 0.6;
                    if (orderTypeEnum == OrderType.coffee) {
                        this._orderTypeText = "coffee";
                    } else if (orderTypeEnum == OrderType.hotCoffee) {
                        this._orderTypeText = "extra hot tea";
                    } else {
                        this._orderTypeText = "orange juice";
                    }
                    break;
                case orderTypeEnum == OrderType.chocolate || orderTypeEnum == OrderType.hotChocolate:
                    this._price = 0.5;
                    if (orderTypeEnum == OrderType.chocolate) {
                        this._orderTypeText = "chocolate";
                    } else {
                        this._orderTypeText = "extra hot chocolate";
                    }
                    break;
                case orderTypeEnum == OrderType.message:
                    this._orderTypeText = "message";
                    break;
                default:
                    break;
            };
        } else {
            this._orderTypeText = "uknown order type";
            this.orderTypeCode = OrderType.error;
        }
    }

    get price() {
        return this._price;
    }

    get sugar() {
        return this._numberOfSugar;
    }

    set sugar(numberOfSugar) {
        if (numberOfSugar && numberOfSugar > 0) {
            this._hasStick = true;
            if (numberOfSugar > 2) {
                this._numberOfSugar = 2;
            }
            else {
                this._numberOfSugar = numberOfSugar;
            }
        } else {
            this._hasStick = false;
            this._numberOfSugar = 0;
        }
    }

    get hasStick() {
        return this._hasStick;
    }

    get message() {
        return this._message;
    }

    set message(message) {
        this._message = message;
    }

};