export const substraction = (number1, number2) => {
    return Math.abs((number1 * 100 - number2 * 100) / 100);
}

export const addition = (number1, number2) => {
    return (number1 * 100 + number2 * 100) / 100;
}

export const multiplication = (number1, number2) => {
    return (number1 * number2 * 100) / 100;
}

export const capitalizeFirstLetter = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}