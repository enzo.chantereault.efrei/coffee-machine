import DrinkMaker from './business/drink-maker.js';

var drinkMaker = new DrinkMaker(1);
var message = drinkMaker.makeOrder("t:3:0.5");

console.log("Drink maker:", message);
drinkMaker.printReports();
